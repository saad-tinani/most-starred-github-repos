# MostStarredGithubRepos

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.7.

## Idea of the App

This is a small web app that gets the most starred Github repositories created in the last 30 days, and displays them as a list (one repository per row) with the infinite scrolling feature in order to get new results while the user keeps scrolling.

**\>> [DEMO HERE](https://most-starred-github-repos.herokuapp.com/) <<**

## Development server

- First of all, clone the repository `most-starred-github-repos` using the command : `git clone https://saad-tinani@bitbucket.org/saad-tinani/most-starred-github-repos.git`;
- Install the app dependencies using the command : `cd most-starred-github-repos && npm install`
- Run `ng serve` for a dev server then navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

# Project structure

The folder structure of a project will change based on a broad range of factors. This is a simple application so I chose this simple structure with a single module :

> **Note:** The **[+]** indicates that the folder has additional files.

    | app

        |-- components

            |-- [+] header

            |-- [+] repository

        |-- interceptors

            |-- http-error.interceptor.ts

        |-- [+] models

        |-- pages

            |-- [+] not-found

            |-- [+] repositories

        |-- pipes

            |-- short-number.pipe.ts

            |-- time-ago.pipe.ts

        |-- services

            |-- [+] repositories

        |-- app-routing.module.ts

        |-- app.component.css

        |-- app.component.html

        |-- app.component.ts

        |-- app.module.ts

    |-- assets

        |-- layout

            |-- [+] img

            |-- [+] svg

## App folder :

### AppModule

Every Angular application has one root module which is conventionally known as the AppModule. The application is being launched by bootstrapping the `AppModule`. While commercial apps have one root module `AppModule` and a number of feature modules, most small apps might contains just one module `AppModule`.

### Components folder

The `components` folder contains all the “shared” components that will be just part of a bigger component. These are components like loaders, buttons and inputs.

### Pages folder

The `pages` folder contains components that will act as an entire view (it may have nested components).

### Interceptors folder

**Angular 4+** introduced a long-awaited feature for handling HTTP requests, the `HttpInterceptor` interface. It provides a way to intercept HTTP requests and responses from our API calls to transform or handle them before passing them along.

The `interceptors` folder is a collection of interceptors used in the application. In our case it contains just the `HttpErrorInterceptor` that will automatically handle errors of all the HTTP requests. We are even able to add the `retry(3)` function to our interceptor, so all http requests will be retried thrice before failing.

Now that we have this in one place, if we want to modify the way we handle our errors, we can simply go to this one file and update it globally across our application.

### Models folder

The `models` folder contains models (into which the JSON objects will be mapped) used across the application.

### Pipes folder

The `pipes` folder contains pipes used across the application :

-  `short-number.pipe.ts` : Converts big numbers to short numbers with suffix (K thousands; M million; B billion; and T trillion and so one…);
-  `time-ago.pipe.ts` : Get time interval between the current and the given date then display it with a time unity (second; minute; hour; day; week; month and year).

### Services folder

The `services` folder contains all the singleton services of the application.

The `RepositoriesService` is the service that we use to call the Github API by the method `getRepositories()`.

## Assets folder :

The `assets` folder contains the global styles of the project, fonts, translation files, images or icons.

In our case it contains one folder named `layout` that contains two folders :

-  `img` : it contains images;
-  `svg` : it contains SVG files.

# Libraries / Packages

## Bootstrap v4.5.0

There is a lot of frameworks to help you design websites faster and easier. I choosed `Bootstrap` because of many reasons :

- It's the most popular framework for creating layouts;
- It's responsive CSS adjusts to phones, tablets, and desktops;
- It's compatible with all modern browsers;
- It's easy to set up.

You can read more about `Bootstrap` over [here](https://getbootstrap.com/docs/4.4/getting-started/introduction/).

## Moment.js v2.26.0

`Moment.js` is a free and open source JavaScript library that removes the need to use the native `JavaScript Date Object` directly. The library is a wrapper for the Date object making the object a whole lot easier to work with. It provides almost everything that a frontend developer may use in an application.

You can read more about `Moment.js` over [here](https://momentjs.com/).

## Angular Infinite Scroll (ngx-infinite-scroll)

The `infinite-scrolling` effect is when a page loads new data continuously as you scroll down the page.

In order to to add infinite scrolling to our application, I choosed the most popular package made for angular `ngx-infinite-scroll`.

You can read more about `ngx-infinite-scroll` over [here](https://www.npmjs.com/package/ngx-infinite-scroll).

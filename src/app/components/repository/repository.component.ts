import { Component, OnInit, Input } from '@angular/core';
import { Repository } from 'src/app/models/repository.model';

@Component({
  selector: 'repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.css']
})
export class RepositoryComponent implements OnInit {

  @Input() repository: Repository;

  constructor() { }

  ngOnInit(): void {
  }

}

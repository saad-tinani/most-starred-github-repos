import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RepositoriesComponent } from './pages/repositories/repositories.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  { path: '', component: RepositoriesComponent, pathMatch: 'full' },
  { path: 'repositories', component: RepositoriesComponent },
  { path: '404', component: NotFoundComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

// Export components using one object to import it in the AppModule in order to reduce the imports
export const RoutingComponents = [
  RepositoriesComponent,
  NotFoundComponent
];

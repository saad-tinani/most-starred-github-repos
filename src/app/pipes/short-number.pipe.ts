import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'shortNumber'
})
export class ShortNumberPipe implements PipeTransform {

    transform(number: number, args?: any): any {

        // will only work value is a number
        if (isNaN(number)) return null; 
        if ((number === null) || (number === 0)) return 0;

        let unit: string = '';
        let absValue: number = Math.abs(number);
        const rounder: number = Math.pow(10, 1);

        const powers: {unit: string, value: number}[] = [
            {unit: 'K', value: Math.pow(10, 3)},
            {unit: 'M', value: Math.pow(10, 6)},
            {unit: 'B', value: Math.pow(10, 9)},
            {unit: 'T', value: Math.pow(10, 12)}
        ];

        for (let i = 0; i < powers.length; i++) {

            let reduced: number = absValue / powers[i].value;
            
            reduced = Math.round(reduced * rounder) / rounder;

            if (reduced >= 1) {
                absValue = reduced;
                unit = powers[i].unit;
                break;
            }

        }

        return (number < 0 ? '-' : '') + absValue + unit;

    }

}
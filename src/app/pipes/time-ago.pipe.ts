import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
    name: 'timeAgo'
})
export class TimeAgoPipe implements PipeTransform {

    transform(date: string, args?: any): string {

        if (date) {

            // return time interval between the current and the given date 
            return moment(date).fromNow();

        }

        return "";

    }

}
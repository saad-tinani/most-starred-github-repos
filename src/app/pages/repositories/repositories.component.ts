import { Component, OnInit } from '@angular/core';
import { RepositoriesService } from 'src/app/services/repositories/repositories.service';
import { Repository } from 'src/app/models/repository.model';
import * as moment from 'moment';
import { HttpError } from 'src/app/models/http-error.model';

@Component({
  selector: 'app-repositories',
  templateUrl: './repositories.component.html',
  styleUrls: ['./repositories.component.css']
})
export class RepositoriesComponent implements OnInit {

  /** The current date */ 
  public currentDate: string = moment().format("YYYY-MM-DD").toString();

  /** Here we push the list of repositories we get from the Github API */ 
  public repositories: Repository[];

  /** The targeted creation date in order to get the list of repositories created in the last 30 days */ 
  public targetedCreationDate: string = moment(this.currentDate).subtract(30, "days").format("YYYY-MM-DD").toString();
  
  /** Loading status */
  public isLoading: boolean = false;

  /** Error message */
  public httpErrorResponse: HttpError;

  /** The current page number */
  public currentPage: number = 1;

  constructor(private _repositoriesService: RepositoriesService) { }

  ngOnInit(): void {
    this.getRepositories(this.currentPage, this.targetedCreationDate);
  }

  /**
	 * Subscribe to the method getRepositories() of the RepositoriesService in order to get the list of repositories from the Github API
	 * @param page - The page number
	 * @param targetedCreationDate - The targeted creation date of repositories
	 * @param sort - The way the results will be sorted (number of stars, forks, or help-wanted-issues)
	 * @param order - The results order : determines whether the first search result returned is the highest number of matches (desc) or lowest number of matches (asc)
	 */
  getRepositories(page: number, targetedCreationDate: string, sort: string = "stars", order: string = "desc"): void {
    this.isLoading = true; // Start loading

    this._repositoriesService.getRepositories(page, targetedCreationDate, sort, order).subscribe(
      (response) => {

          if (this.repositories) {
            this.repositories = this.repositories.concat(response.items);
          } else {
            this.repositories = response.items;
          }

          this.httpErrorResponse = null;
          this.isLoading = false; // Stop loading

      }, (error) => {

        console.error("getRepositories() =>", error);

        this.httpErrorResponse = error;
        this.isLoading = false; // Stop loading

      }
    );
  }
  
  onScroll(): void {
    this.currentPage++;
    this.getRepositories(this.currentPage, this.targetedCreationDate);
  }

}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Repository } from 'src/app/models/repository.model';
import { GetRepositoriesResponse } from 'src/app/models/get-repositories-response.model';

@Injectable({
  providedIn: 'root'
})
export class RepositoriesService {

  constructor(private _httpClient: HttpClient) { }

	/**
	 * Get the most starred Github repositories
	 * @param page - The page number
	 * @param targetedCreationDate - The targeted creation date of repositories
	 * @param sort - The way the results will be sorted (number of stars, forks, or help-wanted-issues)
	 * @param order - The results order : determines whether the first search result returned is the highest number of matches (desc) or lowest number of matches (asc)
	 */
	getRepositories(page: number, targetedCreationDate: string, sort: string, order: string): Observable<GetRepositoriesResponse> {
		return this._httpClient.get<GetRepositoriesResponse>(`https://api.github.com/search/repositories?q=created:>${targetedCreationDate}&sort=${sort}&order=${order}&page=${page}`)
			.pipe(
				// Emits only the first value emitted by the source Observable
				take(1),

				// Map the JSON result into GetRepositoriesResponse model
				map(response => new GetRepositoriesResponse().deserialize(response))
			);
	}

}

// export interface GetRepositoriesResponse {
//     publictotal_count: number;
//     incomplete_results: boolean;
//     items: Repository[];
// }
import { Deserializable } from "./deserializable.model";
import { Repository } from './repository.model';

export class GetRepositoriesResponse implements Deserializable {

    public publictotal_count: number;
    public incomplete_results: boolean;
    public items: Repository[];

    constructor(
        publictotal_count: number = 0,
        incomplete_results: boolean = true,
        items: Repository[] = []
    ) {
        this.publictotal_count = publictotal_count;
        this.incomplete_results = incomplete_results;
        this.items = items;
    }

    deserialize(input: any): this {
        Object.assign(this, input);

        if (input.items) {
            this.items = input.items.map(item => new Repository().deserialize(item));
        } else {
            this.items = [];
        }

        return this;
    }

}
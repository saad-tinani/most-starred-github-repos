import { Deserializable } from "./deserializable.model";
import { Owner } from './owner.model';

export class Repository implements Deserializable {

    public id: number;
    public name: string;
    public description: string;
    public html_url: string;
    public owner: Owner;
    public stargazers_count: number;
    public stargazers_url: string;
    public open_issues_count: number;
    public open_issues_url: string;
    public created_at: string;

    constructor(
        id: number = 0,
        name: string = "",
        description: string = "",
        html_url: string = "",
        owner: Owner = new Owner(),
        stargazers_count: number = 0,
        stargazers_url: string = "",
        open_issues_count: number = 0,
        open_issues_url: string = "",
        created_at: string = ""
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.html_url = html_url;
        this.owner = owner;
        this.stargazers_count = stargazers_count;
        this.stargazers_url = stargazers_url;
        this.open_issues_count = open_issues_count;
        this.open_issues_url = open_issues_url;
        this.created_at = created_at;
    }

    deserialize(input: any): this {
        Object.assign(this, input);

        // Create links to the repository stargazers/issues
        this.stargazers_url = input.html_url + '/stargazers';
        this.open_issues_url = input.html_url + '/issues';

        // Map the owner JSON object into owner model
        if (input.owner)
            this.owner = new Owner().deserialize(input.owner);
        else
            this.owner = new Owner();
            
        return this;
    }

}
import { Deserializable } from "./deserializable.model";

export class Owner implements Deserializable {

    public id: number;
    public login: string;
    public avatar_url: string;
    public html_url: string;

    constructor(
        id: number = 0,
        login: string = "Anonymous",
        avatar_url: string = "assets/layout/img/default_avatar.jpg",
        html_url: string = "#"
    ) {
        this.id = id;
        this.login = login;
        this.avatar_url = avatar_url;
        this.html_url = html_url;
    }

    deserialize(input: any): this {
        Object.assign(this, input);

        this.avatar_url = input.avatar_url && (input.avatar_url !== "") ? input.avatar_url : 'assets/layout/img/default_avatar.jpg';

        return this;
    }

}
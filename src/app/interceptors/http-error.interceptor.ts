import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { HttpError } from '../models/http-error.model';
import { Router } from '@angular/router';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

    constructor(private _router: Router) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        return next.handle(request)
            .pipe(
                // Every http request will be retried thrice before failing
                retry(3),

                // Handle the error
                catchError((error: HttpErrorResponse) => {

                    let httpError: HttpError;

                    // Handle the error based on the HTTP response status code
                    switch (error.status) {
                        case 404:
                            this._router.navigateByUrl('/404');
                            break;
                        
                        case 401:
                            httpError = new HttpError(error.status, "Sorry, but you don't have permission to access this page!");
                            break;

                        case 500:
                            httpError = new HttpError(error.status, "Internal Server Error!");
                            break;
                    
                        default:
                            httpError = new HttpError(0, "Something went wrong...");
                            break;
                    }

                    return throwError(httpError);

                })
            );

    }
}
const express = require('express');
const app = express();

app.use(express.static('./dist/most-starred-github-repos'));

app.get('/*', function(req, res) {
    res.sendFile('index.html', {root: 'dist/most-starred-github-repos/'}
  );
});

app.listen(process.env.PORT || 8080);